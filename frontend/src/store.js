/**
 * Vuex
 * http://vuex.vuejs.org/zh-cn/intro.html
 */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex, axios);

const now = new Date();
const store = new Vuex.Store({
    state: {
        // current user
        user: {
            name: 'Blibli.com',
            img: 'dist/images/1.jpg'
        },
        // session list
        sessions: [
            {
                id: 1,
                user: {
                    name: 'Mail',
                    img: 'dist/images/2.png'
                },
                messages: [
                    {
                        content: 'min',
                        date: now
                    }, {
                        content: 'Ini kenapa tidak bisa pay with transfer?',
                        date: now,
                        self: true
                    }
                ]
            }
            // {
            //     id: 2,
            //     user: {
            //         name: 'Mail',
            //         img: 'dist/images/2.png'
            //     },
            //     messages: [
            //         {
            //             content: 'min',
            //             date: now
            //         }
            //     ]
            // },
        ],
        // currently selected session
        currentSessionId: '',
        // filter out session that only contain this key
        filterKey: ''
    },
    mutations: {
        getMsg(state){
            const URL = 'https://us-central1-primal-outrider-189712.cloudfunctions.net/blibli-get-all-messages'
            axios.get(URL)
                .then(response => {
                    // console.log(response.data)
                    var result = response.data
                    for(var i = 0; i < state.sessions.length ; i++){
                        let pesan = state.sessions.find(result => result.sender === state.sessions[i].id)
                        state.sessions[i].messages = pesan
                    }
                }).catch(err => {
                console.log(err)
            })

        },
        async getUser(state){
            const hasilUser = await axios.get('https://us-central1-primal-outrider-189712.cloudfunctions.net/blibli-get-all-user')
            const URL = 'https://us-central1-primal-outrider-189712.cloudfunctions.net/blibli-get-all-messages'
            const hasilMessages = await axios.get(URL)

            var newSessions = []

            var userList = []
            for(var i = 0; i < hasilUser.data.length ; i++){
                const hasilProfil = await axios.post('https://us-central1-primal-outrider-189712.cloudfunctions.net/blibli-get-user-profile',{id:hasilUser.data[i].id})

                let userData = {
                    name: hasilProfil.data.displayName,
                    img: hasilProfil.data.pictureUrl
                }
                userList.push(userData)
            }

            var result = hasilMessages.data
            for(var i = 0; i < hasilUser.data.length ; i++){
                let temp = hasilUser.data[i].id
                let pesan = result.filter(function (obj) {
                    if(obj.sender === temp){
                        return true
                    }
                    return false
                })

                let a = {
                    id: hasilUser.data[i].id,
                    user: {
                        name: userList[i].name,
                        img: userList[i].img
                    },
                    messages: pesan
                }
                newSessions.push(a)
            }

            state.sessions = newSessions
        },
        INIT_DATA (state) {
            let data = localStorage.getItem('vue-chat-session');
            if (data) {
                state.sessions = JSON.parse(data);
            }
        },
        // send a message
        SEND_MESSAGE ({ sessions, currentSessionId }, content) {
            const URL = 'https://us-central1-primal-outrider-189712.cloudfunctions.net/blibli-line-insert'
            var data = {
                receiver : currentSessionId,
                timestamp : (new Date).getTime(),
                message : content
            }

            axios.post(URL, data, {headers: {
                'Content-Type': 'application/json'
                }})
                .then(response => {
                    // console.log(response)
                }).catch(err => {
                console.log(err)
            })

            var dataLine = {
                receiver: currentSessionId,
                message: content
            }
            console.log(dataLine)
            const URLLine = 'https://us-central1-primal-outrider-189712.cloudfunctions.net/blibli-line-push'
            axios.post(URLLine, dataLine, {headers: {
                'Content-Type': 'application/json'
                }})
                .then(response => {
                    console.log(response)
                }).catch(err => {
                    console.log(err)
            })

            let session = sessions.find(item => item.id === currentSessionId);
            session.messages.push({
                content: content,
                date: new Date(),
                self: true
            });
        },
        // select session
        SELECT_SESSION (state, id) {
            state.currentSessionId = id;
        } ,
        // search for
        SET_FILTER_KEY (state, value) {
            state.filterKey = value;
        }
    }
});

store.watch(
    (state) => state.sessions,
    (val) => {
        // console.log('CHANGE: ', val);
        localStorage.setItem('vue-chat-session', JSON.stringify(val));
    },
    {
        deep: true
    }
);

export default store;
export const actions = {
    getMessages: ({ dispatch }) => dispatch("getUser"),
    initData: ({ dispatch }) => {
        dispatch('getUser')
        // dispatch('getMsg')
    },
    sendMessage: ({ dispatch }, content) => dispatch('SEND_MESSAGE', content),
    selectSession: ({ dispatch }, id) => dispatch('SELECT_SESSION', id),
    search: ({ dispatch }, value) => dispatch('SET_FILTER_KEY', value)
};
