// polyfill
import 'babel-polyfill';

import Vue from 'vue';
import App from './App';
import store from './store';

// require('../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss')

Vue.config.devtools = true;

new Vue({
    el: 'body',
    components: { App },
    store: store
});
