const request = require('request');

exports.helloWorld = (req, res) => {
  
  res.set('Access-Control-Allow-Origin', "*")
 	res.set('Access-Control-Allow-Methods', 'GET, POST')
   res.set('Access-Control-Allow-Headers', '*')
   res.set('Access-Control-Allow-Credentials', 'true')
  
  var headersContent = '{"Authorization" : "Bearer {iaY32lRQ/OoDd269yhjEAup2Io7J96xqFlgGWhHzPMcFM4v+uAEXqFUMHeletV044pYv7A68MdXok7DPclzCSrEzJenBUvW4JzG4hOX9KL2LwaDQ5GngIkn/pDA8sGql91x/tqT8zn117H8q1Jf23QdB04t89/1O/w1cDnyilFU=}"}'
  
  request.get({
      headers: JSON.parse(headersContent), 
      url: 'https://api.line.me/v2/bot/profile/' + req.body.id 
    }, function (error, response, body){
  		res.send(body);
    });
};
